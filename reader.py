from nltk.corpus.reader.plaintext import PlaintextCorpusReader
#from nltk.corpus.reader.api import CorpusReader
from nltk.corpus import TwitterCorpusReader
from topsy_reader import TopsyCorpusReader
#from nltk.tokenize import TweetTokenizer
from json_reader import JsonCorpusReader
from tweetscsv_reader import TweetsCsvCorpusReader

def CorpusBuilder(corpus_path, corpus_type):
	if corpus_type == "Plain":
		return PlaintextCorpusReader(corpus_path, ".*")
	
	if corpus_type == "Twitter":
		return TwitterCorpusReader(corpus_path, '.*\.json')
	
	if corpus_type == "Topsy":
		return TopsyCorpusReader(corpus_path, '.*\.json')
	
	if corpus_type == "JSON":
		return JsonCorpusReader(corpus_path, '.*\.json')

	if corpus_type == "TweetsCsv":
		return TweetsCsvCorpusReader(corpus_path, '.*\.csv')







