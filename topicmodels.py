from gensim import corpora, models
import gensim

def ldamodel(corpus, k):
	texts = corpus.tokenized(corpus.fileids())
	dictionary = corpora.Dictionary(texts)	# assign ids
	gensim_corpus = [dictionary.doc2bow(text) for text in texts]	# create bag-of-words corpus with ids and counts
	return gensim.models.ldamodel.LdaModel(gensim_corpus, num_topics=k, id2word=dictionary)
	
"""
def ldatopics(corpus, k):
	texts = corpus.tokenized(corpus.fileids())
	dictionary = corpora.Dictionary(texts)	# assign ids
	gensim_corpus = [dictionary.doc2bow(text) for text in texts]	# create bag-of-words corpus with ids and counts
	ldamodel = gensim.models.ldamodel.LdaModel(gensim_corpus, num_topics=k, id2word=dictionary)
#	return ldamodel.print_topics(num_topics=k, num_words=20)
	return ldamodel.show_topics(num_topics=k, num_words=20, formatted=False)
"""

def ldatopics(ldamodel, k):
	return ldamodel.show_topics(num_topics=k, num_words=20, formatted=False)
"""
def sort_words(lda_topics):
	k = len(lda_topics)
	for i in range(0,k):
		topic_tuples = lda_topics[i]
		sorted_words = sort(topic_tuples, key=def getScore(tupl): return word_score(tupl,k), reverse=True)

def sorted_beta(ldamodel):
	log_beta = gensim.models.ldamodel.LdaState.getElogbeta()


def word_score(beta_matrix):
	beta = math.exp()
	return beta * (beta - (1/k)*sum()
"""

