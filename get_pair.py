import subprocess
import sys
import re

def run_cmd(cmd, text):
	out = subprocess.check_output([cmd, text], stderr = subprocess.STDOUT)
	return out

# segment different sentences in the output
def sentence_seg(s):
	seg = [i for i in range(len(s)) if s.startswith("Parsing [sent.", i)]
	last = len(s)
	ret = []
	for i in reversed(seg):
		ret.insert(0, s[i : last-1])
		last = i
	return ret


def get_pair(s):
	tree_start = s.find("(ROOT") 
	lines = s[tree_start:].split('\n')
	count = 0
	while len(lines[count]) == 0 or lines[count][0] in ['(', ' ']:
		count += 1

	subj_pairs = []
	obj_pairs = []
	# locate dependencies part
	while len(lines[count]) != 0:
		line = lines[count]
		dep = line[:line.find('(')]
		if dep.find("subj") != -1:
			verb = line[line.find('(')+1 : line.find('-')]
			subj = line[line.find(',')+2 : line.rfind('-')]
			subj_pairs.append((verb, subj))
		elif dep.find("obj") != -1:
			verb = line[line.find('(')+1 : line.find('-')]
			obj = line[line.find(',')+2 : line.rfind('-')]
			obj_pairs.append((verb, obj))
		count += 1

	print subj_pairs
	print obj_pairs



cmd = """../slp/lexparser.sh"""


if __name__ == "__main__":

	if len(sys.argv) < 2:
		sys.exit(1)
	text = sys.argv[1]
	output = run_cmd(cmd, text)

	segment = sentence_seg(output)
	for seg in segment:
		get_pair(seg)
		print