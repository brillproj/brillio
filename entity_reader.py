import csv
from reader import CorpusBuilder
import json
from nltk.util import ngrams

def load_merchants(file_path):
	""" reads merchants from the csv file"""
	with open(file_path) as f:
		csvreader = csv.DictReader(f)
		merchants = set()
#		csvreader.next()
		for row in csvreader:
			try:	
				merchant = row['Aggregated\nMerchant'].lower()
				if len(merchant) > 2:
					merchants.add(merchant)
			except:
				pass

	return merchants		

def load_banks(file_path):
	""" loads banks from line-delimited file"""
	banks = set()
	with open(file_path) as f:
		for line in f:
			bank = line.strip('\n').lower()
			if len(bank)>2:
				banks.add(bank)
	return banks

def replace_entities(corpus, entity_sets, entity_names, out_file_path):
	""" 
	replaces entities with their names
	entity_sets: list of sets (one set for each entity)
	entity_names: list of names (one name for each entity) 
	outputs corpus in json format 	
	"""
	texts = corpus.strings(corpus.fileids())
	out_file = open(out_file_path, mode='w')
	for text in texts:
		if len(text)>5:
			text = text.lower()
			for i in range(0, len(entity_sets)):
				entity_set = entity_sets[i]
				entity_name = entity_names[i]
				for entity in entity_set:
					if entity in text:
						text = text.replace(entity, entity_name)
			text_dict = {}
			text_dict['text'] = text
			out_file.write(json.dumps(text_dict) + '\n')
	out_file.close()	


if __name__=="__main__":
	merchants = load_merchants('data/contactless_merchants_2Q_15.csv')
	print 'number of merchants: ' + str(len(merchants))
	#fw = open('data/merchants_only.txt', mode='w')
	#fw.write('\n'.join(merchants))
	#fw.close()
	banks = load_banks('data/financial_institutions.txt')
	#print banks
	
	dir_path = "data/Tweets_Csv/"
	corpus = CorpusBuilder(dir_path, "TweetsCsv")
	replace_entities(corpus, [merchants, banks], ['merchant', 'bank'], 'output/tweets_entity_replaced.json')
	
