``_`` \_`` ''_'' Welcome_NNP to_TO r/London_NNP London_NNP Wiki_NNP Everything_NNP you_PRP need_VBP to_TO know_VB about_IN visiting_VBG ''_'' }_:
``_`` \_`` ''_'' I_PRP added_VBD my_PRP$ cards_NNS to_TO my_PRP$ iPhone_NN passbook_NN today_NN in_IN the_DT hope_NN of_IN paying_VBG using_VBG my_PRP$ phone_NN for_IN the_DT first_JJ time_NN ._.
I_PRP went_VBD to_TO get_VB my_PRP$ lunch_NN at_IN Boots_NNPS (_: supposedly_RB one_CD of_IN the_DT Apple_NNP Pay_NNP shops_NNS )_: ''_'' }_:
``_`` My_PRP$ boss_NN used_VBN his_PRP$ Apple_NNP Watch_NNP on_IN the_DT tube_NN this_DT morning_NN ._.
Source_NN :_: He_PRP wo_MD n't_RB stop_VB talking_VBG about_IN it_PRP ``_`` }_:
``_`` \_`` ''_'' I_PRP 've_VBP been_VBN using_VBG it_PRP all_DT day_NN ._.
The_DT weirdness_NN is_VBZ working_VBG out_RP how_WRB to_TO tell_VB the_DT cashier_NN you_PRP want_VBP to_TO pay_VB contactless_NN ;_: they_PRP 're_VBP used_VBN to_TO seeing_NN your_PRP$ card_NN and_CC wo_MD n't_RB do_VB anything_NN when_WRB they_PRP see_VBP you_PRP waving_VBG your_PRP$ phone_NN about_IN ._.
Also_RB ''_'' }_:
``_`` Yeah_NNP ._.
My_PRP$ cashier_NN was_VBD amazed_VBN and_CC asked_VBD me_PRP what_WP app_NN it_PRP was_VBD ._.
Great_NNP tip_NN for_IN the_DT tube_NN ._.
I_PRP worked_VBD out_RP you_PRP could_MD possibly_RB do_VB it_PRP that_IN way_NN round._NNP ``_`` }_:
``_`` I_PRP just_RB got_VBD it_PRP to_TO work_VB first_RB time_VB at_IN the_DT station_NN ._.
I_PRP guess_VBP it_PRP was_VBD just_RB the_DT Boots/Ingenico_NNP terminal_JJ that_IN was_VBD n't_RB working._NNP ``_`` }_:
``_`` Been_VBN wearing_VBG a_DT bpay_NN for_IN a_DT few_JJ months_NNS ._.
Point_NN at_IN the_DT contactless_NN reader_NN and_CC they_PRP usually_RB turn_VBP it_PRP on._NNP ``_`` }_:
``_`` Shortcut_IN iOS_NNP 9_CD :_: Double_JJ tap_NN the_DT home_NN key_NN from_IN the_DT lock_NN screen_NN (_: phone_NN either_CC while_IN awake_NN or_CC asleep_VB )_CD to_TO load_VB your_PRP$ card_NN ._.
I_PRP 'm_VBP always_RB making_VBG sure_NN I_PRP authenticate_VBP before_IN I_PRP get_VBP to_TO the_DT gates_NNS ._.
Waiting_VBG for_IN your_PRP$ phone_NN to_TO detect_VB the_DT reader_NN is_VBZ a_DT sure_NN fire_NN way_NN to_TO piss_NNS off_IN everyone_NN behind_IN you._NNP ``_`` }_:
``_`` \_`` ''_'' No_DT ''_'' }_:
``_`` Haha_NNP -_: but_CC imagine_NN how_WRB much_JJ fun_NN you_PRP 'll_MD have_VB paying_VBG for_IN things_NNS then_RB stepping_VBG into_IN your_PRP$ flying_NN car_NN !_. ``_`` }_:
``_`` \_`` ''_'' I_PRP have_VBP n't_RB got_VBN a_DT licence_NN ''_'' }_:
``_`` Will_NNP be_VB interesting_JJ to_TO see_VB how_WRB soon_RB they_PRP adopt_VBP ApplePay_NNP considering_VBG they_PRP 've_VBP just_RB launched_VBN bPay_NNP which_WDT could_MD be_VB a_DT good_JJ moneymaker_NN for_IN them._NNP ``_`` }_:
``_`` Well_NNP they_PRP do_VBP offer_VBP bPay_NNP ``_`` }_:
``_`` Used_VBN it_PRP this_DT morning_NN to_TO tap_VB in_IN and_CC out_IN of_IN my_PRP$ tube_NN journey_NN ._.
Worked_NNP fine_NN but_CC a_DT slight_NN delay_NN compared_VBN to_TO an_DT oyster_NN card._NNP ``_`` }_:
``_`` Me_PRP too_RB ``_`` }_:
``_`` You_PRP get_VBP a_DT slight_NN delay_NN with_IN using_VBG a_DT contactless_NN card_NN too_RB ._.
Anybody_NNP know_VBP if_IN it_PRP 's_VBZ the_DT same_JJ amount_NN of_IN delay_NN ?_. ``_`` }_:
``_`` \_`` ''_'' I_PRP think_VBP it_PRP 's_VBZ the_DT same_JJ amount_NN of_IN delay_NN ''_'' }_:
``_`` \_`` ''_'' With_IN Oyster_NNP you_PRP can_MD just_RB walk_VB through_IN the_DT gate_NN as_IN if_IN it_PRP was_VBD open_JJ ''_'' }_:
``_`` worked_VBD fine_NN for_IN me_PRP today_NN in_IN smiths_NNS !_.
are_VBP you_PRP sure_RB you_PRP 're_VBP on_IN a_DT iphone_NN 6_CD or_CC 6+_CD ?_.
the_DT other_JJ iphones_NNS do_VBP n't_RB have_VB it_PRP -_: I_PRP think_VBP you_PRP also_RB have_VBP to_TO have_VB set_VBN up_RP a_DT passcode_NN and_CC touch_JJ ID_NNP ``_`` }_:
``_`` \_`` ''_'' Yeah_NNP I_NNP was_VBD using_VBG an_DT iPhone_NNP 6_CD with_IN the_DT latest_JJS OS_NNP ''_'' }_:
``_`` What_WP bank_NN are_VBP you_PRP with_IN ?_.
HSBC_NNP (_: and_CC therefore_RB First_NNP direct_JJ too_RB )_: pulled_VBN out_RP early_RB this_DT morning_NN ._.
Otherwise_RB it_PRP might_MD be_VB a_DT faulty_NN reader_NN -_: try_NN one_CD of_IN the_DT self_NN scan_NN ones_NNS next_JJ time_NN so_IN you_PRP do_VBP n't_RB embarass_VB yourself_PRP !_. ``_`` }_:
``_`` \_`` ''_'' RBS_NNP so_RB it_PRP should_MD have_VB worked_VBN ._.
Haha_NNP yeah_NN ''_'' }_:
``_`` \_`` ''_'' It_PRP 's_VBZ not_RB down_IN to_TO Apple_NNP Pay_NNP ;_: different_JJ retailers_NNS use_VBP different_JJ readers_NNS ''_'' }_:
``_`` Yeah_NNP I_PRP got_VBD the_DT impression_NN it_PRP was_VBD the_DT NFC_NNP reader_NN rather_RB than_IN the_DT Apple_NNP Pay_NNP part_NN ._.
Still_RB annoying_VBG as_IN the_DT payment_NN hardware/software_JJ providers_NNS will_MD have_VB known_VBN this_DT date_NN was_VBD coming_VBG ..._: ``_`` }_:
``_`` What_WP is_VBZ the_DT advantage/benefit_JJ of_IN using_VBG Apple_NNP Pay_NNP over_IN debit_NN or_CC credit_NN card_NN ?_. ``_`` }_:
``_`` \_`` ''_'' It_PRP 's_VBZ more_JJR secure_NN because_IN the_DT merchant_NN never_RB gets_VBZ your_PRP$ real_JJ card_NN number_NN ._.
You_PRP can_MD carry_VB all_PDT of_IN your_PRP$ cards_NNS on_IN one_CD device_NN ._.
At_IN least_JJS in_IN the_DT immediate_JJ future_JJ you_PRP 're_VBP probably_RB still_RB going_VBG to_TO carry_VB a_DT physical_JJ card_NN too_RB ''_'' }_:
``_`` \_`` ''_'' Apple_NNP watch_VBP has_VBZ been_VBN very_RB easy_JJ today_NN ._.
Only_RB challenge_VBP will_MD be_VB at_IN some_DT bars_NNS where_WRB they_PRP keep_VBP the_DT machine_NN by_IN the_DT till_NN and_CC currently_RB just_RB tap_VB your_PRP$ card_NN and_CC give_VB it_PRP back_RB ._.
I_PRP can_MD hardly_RB give_VB them_PRP my_PRP$ arm_NN !_.
But_CC it_PRP 's_VBZ great_JJ as_IN an_DT Oyster_NNP replacement_NN ''_'' }_:
``_`` \_`` ''_'' No_DT ''_'' }_:
``_`` \_`` ''_'' Thanks_NNP ''_'' }_:
``_`` \_`` ''_'' No_DT ''_'' }_:
``_`` \_`` ''_'' Good_NNP to_TO know_VB ''_'' }_:
``_`` My_PRP$ guess_NN is_VBZ security_NN as_IN you_PRP need_VBP the_DT Touch_NNP ID_NNP to_TO use_VB it_PRP ._.
In_IN the_DT future_NN you_PRP will_MD be_VB able_JJ to_TO carry_VB out_RP High_JJ Value_NNP Payments_NNP (_NNP over_IN \u00a320_-NONE- -_: up_IN to_TO a_DT limit_NN the_DT ship_NN decides_VBZ )_: using_VBG contactless_NN so_IN the_DT phone_NN method_NN would_MD be_VB far_RB more_RBR secure_JJ than_IN a_DT contactless_NN card_NN which_WDT anyone_NN can_MD use_VB if_IN you_PRP lost_VBD it._NNP ``_`` }_:
``_`` \_`` ''_'' Used_NNP it_PRP at_IN Coop_NNP ''_'' }_:
``_`` \_`` ''_'' Used_NNP it_PRP at_IN Pret_NNP ''_'' }_:
``_`` Bought_JJ an_DT Apple_NNP Watch_NNP for_IN 500_CD something_NN with_IN just_RB a_DT tap_NN of_IN my_PRP$ phone_NN ..._: .._: Insane_NNP how_WRB easy_JJ it_PRP is_VBZ getting_VBG to_TO spend_VB money_NN !_. ``_`` }_:
``_`` It_PRP works_VBZ with_IN my_PRP$ American_JJ Express_NNP but_CC does_VBZ not_RB let_VB you_PRP register_JJR a_DT Citibank_NNP debit_NN card_NN yet._NNP ``_`` }_:
``_`` \_`` ''_'' Yup_NNP ''_'' }_:
