from reader import CorpusBuilder
from topicmodels import ldamodel, ldatopics
from pprint import pprint
import operator
import nltk.collocations
import json

def main():
	#dir_path = "/media/data1/misagh/brillio/data/sample_topsy/"
	#corpus = CorpusBuilder(dir_path, "Topsy")
	
#	dir_path = "data/AppleDocuments/parsed/"
#	dir_path = "data/GoogleWallet - Reddit/parsed/"
#	corpus = CorpusBuilder(dir_path, "JSON")
	dir_path = "data/Tweets_Csv/"
	corpus = CorpusBuilder(dir_path, "TweetsCsv")
#	run_lda(corpus, 10)
#	pprint(top_hashtags(corpus, 100))
#	pprint(top_bigrams(corpus, 100))
	write_term_docs(corpus, 'fail', 'output/fail_tweets.txt')
#	write_to_json(corpus, 'data/Tweets_Csv/tweets_parsed.json')

def write_to_json(corpus, file_path):
	texts = corpus.strings(corpus.fileids())
	out_file = open(file_path, mode='w')
	for text in texts:
		if len(text)>5:
			text_dict = {}
			text_dict['text'] = text
			out_file.write(json.dumps(text_dict) + '\n')
	out_file.close()

def run_lda(corpus, k):
#	print corpus.tokenized(corpus.fileids()[0])
#	pprint(ldatopics(corpus, 10)) 
	lda_model = ldamodel(corpus, k)
	topics = ldatopics(lda_model, k)
	topic_words = [[pw[1] for pw in topic] for topic in topics]
	pprint(topic_words)
	
	output_file = 'output/topic_words_' + str(k) + '.txt'
	fw = open(output_file, mode='w')
	for t in topic_words:
		fw.write((', '.join(t)).encode('iso-8859-1') + '\n')
	fw.close()

def top_hashtags(corpus, k):
	""" return top k hashtags with their number of occurrence """
	hashtag_count = {}
	texts = corpus.tokenized(corpus.fileids())
	for text in texts:
		for term in text:
			if term.startswith('#'):
				try:
					hashtag_count[term] += 1
				except:
					hashtag_count[term] = 1
	sorted_hashtags = sorted(hashtag_count.items(), key=operator.itemgetter(1), reverse=True)
	return (sorted_hashtags[1:k])	
	

def write_term_docs(corpus, word, output_file):
	""" write documents containing a specific substring to file"""
	texts = corpus.tokenized(corpus.fileids())
	fw = open(output_file, mode='w')
	for text in texts:
		for term in text:
			if word.lower() in term.lower():
				fw.write(' '.join(text) + '\n')
	fw.close()


def top_bigrams(corpus, k):
	""" returns top bigrams for the corpus """
	bigram_measures = nltk.collocations.BigramAssocMeasures()
	finder = nltk.collocations.BigramCollocationFinder.from_words([item for sublist in corpus.tokenized(corpus.fileids()) for item in sublist])
	return finder.nbest(bigram_measures.likelihood_ratio, k)	


if __name__ == "__main__":
	main()

