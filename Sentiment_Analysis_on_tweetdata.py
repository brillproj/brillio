# -*- coding: cp1252 -*-
from textblob import TextBlob
import csv
import re
import pandas as pd
dir = "D:\\UCLA\\Roy Project\Code_Sentiment_Analysis\data\Tweets_Csv"
file = 'tw.csv'
file_path = dir+"\\"+file
output_file = dir+"\\"+"sentiment_output_tweets.csv"

tweets = pd.DataFrame.from_csv(file_path)['link']
num = len(tweets)
f = open(output_file, 'wb')
writer = csv.writer(f)
All_sentences=[]
for i in range(0,num):
    print i
    url_less_tweet = re.sub(r'(?i)\b((?:http?://|www\d{0,3}[.]|[a-z0-9.\-]+[.][a-z]{2,4}/)(?:[^\s()<>]+|\(([^\s()<>]+|(\([^\s()<>]+\)))*\))+(?:\(([^\s()<>]+|(\([^\s()<>]+\)))*\)|[^\s`!()\[\]{};:\'".,<>?������]))','', tweets[i])
    tb_review = TextBlob(unicode(url_less_tweet, errors='ignore').strip())
    if abs(tb_review.sentiment.polarity)>0.3:
        writer.writerow([tb_review,tb_review.sentiment])
f.close()
